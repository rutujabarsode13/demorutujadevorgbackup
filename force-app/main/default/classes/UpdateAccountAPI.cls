@RESTResource(urlMapping='/UpdateAccount/*')
global with sharing class UpdateAccountAPI 
{
    
    @HttpGet
    global static list<Account> getAccountId()
    {
        RestRequest request = RestContext.request; // object created
        
        String AccID = request.requestURI.substring(request.requestURI.lastIndexOf('/')+1);
        
        list<Account> result = [SELECT id, Name  FROM Account WHERE Id = :AccID];
        return result;

    }
	 //update Account field
      @HttpPut 
      global static wrapperClass updateAccount()
      {
           RestRequest request = RestContext.request;
           String AccId = request.requestURI.substring(request.requestURI.lastIndexOf('/') + 1);
           Account thisAccount = [Select Id from Account where id = :AccId];
           
          Map<String,Object> params = (Map<String,Object>) JSON.deserializeUntyped(request.requestbody.tostring());
          	System.debug(params);
          for(String fieldName : params.keyset()){
              thisAccount.put(fieldName,params.get(fieldName));
         }
            
          wrapperclass wc ;
          try
          {
              update thisAccount;
              wc = new wrapperclass(True,thisAccount.id,'Record updated.');
          }
          catch(Exception e)
          {
              String m = e.getMessage();
               wc = new wrapperclass(False,'',m);
          }
          
          return wc;
      }
}