global class AccountFieldUpdateBatch implements Database.Batchable<sObject>
{
	global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([Select Id,Name from Account]);
        
    }
    global void execute(Database.BatchableContext bc,List<Account>acclist)
    {
        for(Account acc : acclist)
        {
            acc.Batch_Text_Update__c='testing';
        }
        update acclist;
        System.debug('Account field updated');
    }
    global void finish(Database.BatchableContext bc)
    {
      AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id =:BC.getJobId()];
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('Apex Account field update batch email' + a.Status);
       mail.setPlainTextBody('The batch Apex job processed Account field update for ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
    }
    
    
}