public class AccountHandler 
{
     public static Account GetAccount(id accountId)
     {
         if(accountId==Null)
         {
             System.debug('Account id should not be null');
         }
         else
         {
             List<Account> accountidlist = [SELECT Id, Name FROM Account where id=:accountId];
           return accountidlist.get(0);
         }
		return Null;
    }
    public static void GetAccounts(List<id> accountIds)
    {
        List<Account> accountrecordlist = [SELECT Id, Name FROM Account where id IN :accountIds];
       System.debug(accountrecordlist);
       
    }
    public static void GetAccountname(String Accountname)
    {
        List<Account> accountnamelist = [SELECT Id, Name FROM Account where Name=:Accountname];
    }
	public static void UpdateOppStage(set<Id> accids)
    {
        set<id> Accountids = new set<id>();
		List<Opportunity> Opplist = new List<Opportunity>();
	//	System.debug('Trigger map= '+Trigger.newMap.keySet());
	for(Id acc : accids)
    {
       
		Accountids.add(acc);
	}

	for(Opportunity opp : [select id, StageName,Name from Opportunity where Account.id in : Accountids])
	{
		opp.StageName='Closed Lost';
		Opplist.add(opp);

	}
	update Opplist ;
    }
   
}